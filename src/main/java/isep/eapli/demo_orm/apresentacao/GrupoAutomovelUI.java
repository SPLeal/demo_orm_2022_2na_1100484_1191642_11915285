/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.apresentacao;

import isep.eapli.demo_orm.aplicacao.GrupoAutomovelController;
import isep.eapli.demo_orm.dominio.GrupoAutomovel;
import isep.eapli.demo_orm.util.Console;
import java.util.Scanner;

import javax.rmi.CORBA.Util;
import java.util.List;

/**
 *
 * @author mcn
 */
public class GrupoAutomovelUI {

    private final GrupoAutomovelController controller = new GrupoAutomovelController();

    public void registarGA() {
        System.out.println("*** Registo Grupo Automóvel ***\n");
        String nome = Console.readLine("Nome:");
        int portas = Console.readInteger("Número de portas");
        String classe = Console.readLine("Classe:");
        GrupoAutomovel grupoAutomovel = controller.
                registarGrupoAutomóvel(nome, portas, classe);
        System.out.println("Grupo Automóvel" + grupoAutomovel);
    }

    public void listarGAs() {
        controller.listarGruposAutomoveis().forEach(System.out::println);
    }

    public void procurarGAPorID() {
        long id = getLongNumberInput();
        procurarGAPorID(id);
    }


	public void procurarGAPorID(long id) {
		controller.procurarGrupoAutomovel(id);
	}
    //TODO Criar classe própria para utils?
    /**
     * Util function to get long value (insists until valid value given
     * @return long value
     */
    public long getLongNumberInput() {
        Scanner ler = new Scanner(System.in);
        long longNumber;
        do {
            System.out.println("Please enter desired number: ");
            while (!ler.hasNextLong()) {
                System.out.println("Please enter long value");
                ler.next();
            }
            longNumber = ler.nextLong();
        }while(longNumber <= 0);
        return longNumber;
    }
}
