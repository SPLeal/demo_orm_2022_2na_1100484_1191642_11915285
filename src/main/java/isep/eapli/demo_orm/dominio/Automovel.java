package isep.eapli.demo_orm.dominio;

import javax.persistence.*;

@Entity
public class Automovel{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Automovel() {
    }

    @ManyToOne
    private GrupoAutomovel grupoAutomovel;

    private int numKilometros;

    @Column(unique = true)
    private String matricula;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "Foto")
    private  byte[] fotografia;

    public String lerMatricula(){
        return this.matricula;
    }

    public boolean alterarClassAutomovel(GrupoAutomovel novoGrupoAutomovel){
        if (novoGrupoAutomovel == null)
            return false;
        this.grupoAutomovel = novoGrupoAutomovel;
        return true;
    }

    public int alterarNumKilometros(int novoNumKilometros){
        this.numKilometros = novoNumKilometros;
        return novoNumKilometros;
    }

    @Override
    public String toString() {
        return "Automovel{" +
                "grupoAutomovel=" + grupoAutomovel +
                "matricula" + matricula +
                '}';
    }
}
