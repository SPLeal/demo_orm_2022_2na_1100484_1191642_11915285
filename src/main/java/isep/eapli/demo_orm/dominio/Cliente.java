package isep.eapli.demo_orm.dominio;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Cliente {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Enumerated(EnumType.STRING)
    private TipoCliente tipoCliente;

    @OneToMany
    @JoinColumn(name="Contratos de Aluguer")
    private Set<ContratoAluguer> contratos = new HashSet<>();

    public Cliente() {}

    public enum TipoCliente {
        EMPRESA, PRIVADO;
    }

    public boolean criarNovoContrato(ContratoAluguer contrato){
        return contratos.add(contrato);
    }
}
