package isep.eapli.demo_orm.dominio;

import javax.persistence.*;

@Entity
public class CondutorAutorizado {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "contrato_id")
    private ContratoAluguer contrato;


    public CondutorAutorizado(){}

    private String nome;
    private String numeroCartaConducao;

    public CondutorAutorizado(String nome){
        this.nome = nome;
    }
}
