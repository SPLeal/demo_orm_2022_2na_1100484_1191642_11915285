package isep.eapli.demo_orm.dominio;

import javax.persistence.*;
import java.util.*;

@Entity
public class ContratoAluguer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    public ContratoAluguer() {
    }

    @OneToOne
    private Automovel automovel;
    @OneToOne
    private Cliente cliente;

    @Temporal(TemporalType.DATE)
    private Date dataContrato;

    @OneToMany(mappedBy = "contrato", cascade = CascadeType.ALL)
    @JoinColumn(name = "ContratoID")
    private List<CondutorAutorizado> condutoresAutorizados = new ArrayList<CondutorAutorizado>();

    public ContratoAluguer(Automovel automovel, Cliente cliente){
        this.automovel = automovel;
        this.cliente = cliente;
    }

    public boolean adicionarNovoCondutorAutorizado(CondutorAutorizado condutor){
        return condutoresAutorizados.add(condutor);
    }

}
