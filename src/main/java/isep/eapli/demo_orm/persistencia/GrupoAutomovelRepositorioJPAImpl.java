/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.persistencia;

import isep.eapli.demo_orm.dominio.GrupoAutomovel;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author eapli
 */
public class GrupoAutomovelRepositorioJPAImpl extends JpaRepository<GrupoAutomovel, Serializable> {


    @Override
    protected String persistenceUnitName() {
        return "DEMO_ORMPU";
    }

}
